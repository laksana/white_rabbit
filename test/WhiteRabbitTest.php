<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit;

class WhiteRabbitTest extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit */
    private $whiteRabbit;

    public function setUp()
    {
        $this->whiteRabbit = new WhiteRabbit();
        parent::setUp();
    }

    //SECTION FILE !
    /**
     * @dataProvider medianProvider
     */
    public function testMedian($expected, $file){
        $result = $this->whiteRabbit->findMedianLetterInFile($file);
        $this->assertTrue(in_array($result, $expected));
    }

	//Test for text3 will fail because the output will give the median (occurrences order 13 or 14) from the array
	//while the test is expecting output that is off by 2 orders from the median (occurrences order 11 or 12)
	
	//Test for text5 also will fail because the test expecting output that is far from the median
	//which is occurrences order number 2 lowest on the list, not occurrences order 13 or 14 as expected
    
    public function medianProvider(){
        return array(
            array(array(array("letter" => "m", "count" => 9240), array("letter" => "f", "count" => 9095)), __DIR__ ."/../txt/text1.txt"),
            array(array(array("letter" => "w", "count" => 13333), array("letter" => "m", "count" => 12641)), __DIR__ ."/../txt/text2.txt"),
            array(array(array("letter" => "w", "count" => 2227), array("letter" => "g", "count" => 2187)), __DIR__ ."/../txt/text3.txt"),
            array(array(array("letter" => "w", "count" => 3049)), __DIR__ ."/../txt/text4.txt"),
            array(array(array("letter" => "z", "count" => 858)), __DIR__ ."/../txt/text5.txt")
        );
    }
}
