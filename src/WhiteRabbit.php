<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
		if file_exists($filepath){
			$txtFile = strtolower(file_get_contents($filepath));
			return $txtFile;
		}
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
        $arrChar = array(
			"a" => 0, "b" => 0, "c" => 0, "d" => 0, "e" => 0, 
			"f" => 0, "g" => 0, "h" => 0, "i" => 0, "j" => 0, 
			"k" => 0, "l" => 0, "m" => 0, "n" => 0, "o" => 0, 
			"p" => 0, "q" => 0, "r" => 0, "s" => 0, "t" => 0, 
			"u" => 0, "v" => 0, "w" => 0, "x" => 0, "y" => 0, 
			"z" => 0
		);
		foreach ($arrChar as $key => $value){
			$arrChar[$key] = substr_count($parsedFile, $key);
		}
		
		asort($arrChar);
		$mid = (count($arrChar)/2)-1;
		$keyList = array_keys($arrChar);
		$occurrences = $arrChar[$keyList[$mid]];
		return $keyList[$mid];
    }
}